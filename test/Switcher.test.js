/* globals beforeEach, afterEach, describe, it */

const Switcher = require('@lib/switcher').Switcher

describe('switcher Node API', () => {
  const ctrlName = 'test'
  let sw
    
  beforeEach(() => { sw = new Switcher('test', Function.prototype) })

  describe('get shmdata path wrapper', () => {
    const shmdataSuffix = 'video'
    const defaultPath = '/tmp/switcher'

    it('should return the shmdata path when a matching quiddity is found', () => {

      // create quiddity video first
      const vidId = 1
      const vidShmdata = `${defaultPath}_${ctrlName}_${vidId}_${shmdataSuffix}`
      
      sw.create('videotestsrc', 'vid')

      // get shmdata path from quiddities
      expect(sw.get_shmdata_path('vid', shmdataSuffix)).toBe(vidShmdata)
    })

    it('should return an empty string when no matching quiddity exists', () => {
      expect(sw.get_shmdata_path('vid', shmdataSuffix)).toBe('')
    })

    it('should throw an error when there are wrong number of arguments', () => {
      expect(() => {
        sw.get_shmdata_path('vid')
      }).toThrowError(new TypeError('Wrong number of arguments'))
    })

    it('should throw an error when parameters are not strings', () => {
      expect(() => {
        sw.get_shmdata_path('vid', 0)
      }).toThrowError(TypeError('Wrong arguments'))
    })
  })

  describe('load bundles from json config', () => {

    it('should return true if the description is valid JSON', () => {
      let description = 
        '{"bundle": { \
            "videoInput": { \
              "pipeline": "v4l2src name=Capture <shmw <add_to_start", \
              "doc": { \
                  "long_name": "Video Input", \
                  "category": "video", \
                  "tags": "writer", \
                  "description": "Video input from V4L2 device" \
              } \
            } \
          }}'

      expect(sw.load_bundles(description)).toBe(true)
    })

    it('should return false if the description is not valid JSON', () => {
      let description = 'this is wrong'
      expect(sw.load_bundles(description)).toBe(false)
    })

    it('should throw an error when the number of arguments is wrong', () => {
      expect(() => {
        sw.load_bundles()
      }).toThrowError(new TypeError('Wrong number of arguments'))
    })

    it('should throw an error when parameter is not a string', () => {
      expect(() => {
        sw.load_bundles(100)
      }).toThrowError(TypeError('switcher load_bundles: Wrong first argument type'))
    })
  })

  afterEach(() => { sw.close() })
})
