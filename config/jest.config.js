// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

const path = require('path')

module.exports = {
  rootDir: path.resolve(__dirname, '..'),

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // A map from regular expressions to module names that allow to stub out resources with a single module
  moduleNameMapper: {
    '^~(.*)': '<rootDir>/src$1',
    '^@lib(.*)': '<rootDir>/build/Release$1'
  },

  // An array of regexp pattern strings, matched against all module paths before considered 'visible' to the module loader
  modulePathIgnorePatterns: ['node_modules'],

  // The test environment that will be used for testing
  testEnvironment: 'node'
}
