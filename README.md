**THIS PROJECT IS NOW ARCHIVED.**

**Node-Switcher is no longer maintained by the SAT and its use is now deprecated.**
**You can still use Node-Switcher if you wish to run Scenic/Scenic Core 4.0.10 and lower. However, do not expect any help from maintainers.**

# Node-Switcher

Node-Switcher is the native NodeJS add-on for [Switcher](https://gitlab.com/sat-metalab/switcher). It acts as a thin wrapper around Switcher's C++ API, allowing it to be included in Javascript applications. In the context of the Scenic stack, Node-Switcher is the link allowing Scenic Core to communicate with Switcher.

Node-Switcher is currently developed by the [Société des Arts Technologiques [SAT]](http://sat.qc.ca/), a non-profit artistic entity based in Montreal, CA. The current version of Node-Switcher has been tested with **Ubuntu 18.04**.

## Installation

### Dependencies

**IMPORTANT: Take note that Node-Switcher 3.2.x is only compatible with Switcher 2.1.24 and up. Use Node-Switcher 3.1.2 if you plan to use a version of Switcher lower than 2.1.24.**

| Name                             | Version     | Command | Description                                                           |
|----------------------------------|-------------|---------|-----------------------------------------------------------------------|
| [G++/GCC](https://gcc.gnu.org/)  | >= 8        | `sudo apt-get install gcc-8 g++-8` | GNU project C and C++ compiler             |
| [libicu-dev](https://packages.debian.org/fr/sid/libicu-dev) | latest | `sudo apt install libicu-dev` | Development library for handling of unicode characters |
| [Git](https://git-scm.com/)      | latest      | `sudo apt-get install git` | Distributed version control system used to get Node-Switcher source code |
| [NodeJS](https://nodejs.org/en/) | >= 10.15.3 | See [NodeJS installation instructions](https://github.com/nodesource/distributions/blob/master/README.md#debinstall) | JavaScript runtime which helps to build the Scenic engine |
| [NPM](https://www.npmjs.com/)    | >= 5.6.0  | Included with the [NodeJS installation](https://github.com/nodesource/distributions/blob/master/README.md#debinstall) | JavaScript package manager used to build internal dependencies |
| [Shmdata](https://gitlab.com/sat-metalab/shmdata)  | >= 1.3.32 | See [Shmdata documentation](https://gitlab.com/sat-metalab/shmdata) | Library to share flows of data frames between processes via shared memory |
| [Switcher](https://gitlab.com/sat-metalab/switcher)  | >= 2.1.24 | See [Switcher documentation](https://gitlab.com/sat-metalab/switcher) | C++ engine allowing the streaming of data over IP networks |

### Install with Scenic Core

Node-Switcher is specified as a dependency in Scenic Core's ```package.json```. This means that Node-Switcher will be automatically downloaded, built and installed as part of Scenic Core's install process.

See [Scenic Core's installation procedure](https://gitlab.com/sat-mtl/telepresence/scenic-core#install) for more information.

### Install from Source

| Step                               | Command
|------------------------------------|-------------------------
| Clone the Node-Switcher repository | `git clone git@gitlab.com:sat-mtl/telepresence/node-switcher.git`
| Move into Node Switcher folder     | `cd node-switcher`
| Build the app                      | `npm install`

#### Additional Scripts

Additional NPM scripts are available to build the add-on:

```bash
node-gyp configure # configure node-gyp (optionnal)

npm run build
npm run build:debug # build for debug
npm run rebuild
npm run rebuild:debug # rebuild for debug
```

### Install from Docker

The default Docker configuration uses the images built from [Switcher](https://gitlab.com/sat-metalab/switcher/tree/develop/docker). Docker-compose will fetch the **develop** branches from [`shmdata`](https://gitlab.com/sat-metalab/shmdata), [`switcher`](https://gitlab.com/sat-metalab/switcher) and [`node-switcher`](https://gitlab.com/sat-mtl/telepresence/node-switcher).

```bash
# Build the whole stack
docker-compose -f container/docker-compose.yml up
```

#### Build a Docker image

To build a Docker image of Node-Switcher, **you'll need to build the Shmdata and Switcher images beforehand.**.

```bash
# Build node-switcher's image
docker build -t "node-switcher:develop" .
```

The `node-switcher` image uses [mutli-stage builds][docker-multi-stage] with 4 stages : `dependencies`, `nodejs`, `build` and `clean`. Theses stages use some build arguments :

| arguments       | stages              | description                              | default         |
|-----------------|---------------------|------------------------------------------|-----------------|
| SWITCHER_IMAGE  | `dependencies`      | The `switcher` base image                | `registry.gitlab.com/sat-metalab/switcher`
| SWITCHER_TAG    | `dependencies`      | The **tag** of the `switcher` base image | `develop`
| NODEJS_VERSION  | `nodejs`            | NodeJS version to use                    | `10`
| NODEJS_DOWNLOAD | `nodejs`            | Link to the NodeJS binary                | `https://deb.nodesource.com/setup_${NODEJS_VERSION}.x`
| BUILD_DIR       | `build` and `clean` | Where `node-switcher` source is copied   | `/opt/node-switcher`

The `node-switcher` instance could be executed directly with its REPL :

```bash
docker run -ti node-switcher:develop node-switcher-repl
```

## Usage

### Javascript Scripting

Node-Switcher allows you to write *JavaScript* scripts to control Switcher. For example, you can use it to create quiddities, change a quiddity's properties, establish SIP communication with others, etc.

Example:

```js
const Switcher = require('node-switcher').Switcher

const sw = new Switcher('debug', console.log)

// create a new quiddity `jacksrc`
sw.create('jacksrc')
```

You can see all examples in the folder `example`.

### REPL Interface

You can also use a *[Read-Eval-Print Loop (REPL)](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop)* in order to test **Node-Switcher** in the terminal. Type the following command in your terminal to do so:

```bash
# Inside node-switcher/
./bin/repl.js
```

### Debugging

The following Visual Studio Code debug task can help you debug Node-Switcher. lldb and the vadimcn.vscode-lldb VSCode extension must be installed before running this task.

```json
{
  "configurations": [{
    "name": "Node Switcher (lldb)",
    "type": "lldb",
    "request": "launch",
    "program": "${env:HOME}/.bin/node",
    "args": [
      "${workspaceFolder:node-switcher}/examples/debug.js"
    ],
    "preLaunchTask": "npm: build:dev"
  }]
}
```

## Technologies

This add-on uses **C++ 17** and **NodeJS >= 10**.

The SAT uses **[Ubuntu 18.04](http://releases.ubuntu.com/18.04/)**, so testing priority is given to this version.

## Documentation

The NodeJs API is documented with source comments into cpp files. The documentation can be generated with [JSDoc](https://jsdoc.app/) by running the command :

```bash
npm run doc
```

## Contributing

Check out our [Contributing Guide](CONTRIBUTING.md) to get started!

## Versioning

[SemVer](http://semver.org/) is used for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/sat-mtl/telepresence/node-switcher/tags).

## Authors

See [here](AUTHORS.md).

## License

This project is licensed under the GNU General Public License version 3 - see the [LICENSE](LICENSE.md) file for details.

## Acknowledgments

This project was made possible by the [Société des Arts Technologiques [SAT]](http://sat.qc.ca/).
