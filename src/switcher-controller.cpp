/*
 * This file is part of switcher-nodejs.
 *
 * switcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * switcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with switcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./switcher-controller.hpp"

using namespace std;
using namespace v8;
using namespace node;
using namespace switcher;
using namespace quiddity;

Persistent<Function> SwitcherController::constructor;

SwitcherController::SwitcherController(const std::string &name,
                                       Local<Function> logger_callback) {
  Isolate *isolate = Isolate::GetCurrent();

  // keep the log callback
  user_log_cb.Reset(isolate, logger_callback);

  // mutex
  uv_mutex_init(&switcher_log_mutex);
  uv_mutex_init(&switcher_prop_mutex);
  uv_mutex_init(&switcher_sig_mutex);

  // async
  switcher_log_async.data = this;
  uv_async_init(uv_default_loop(), &switcher_log_async, NotifyLog);

  switcher_prop_async.data = this;
  uv_async_init(uv_default_loop(), &switcher_prop_async, NotifyProp);

  switcher_sig_async.data = this;
  uv_async_init(uv_default_loop(), &switcher_sig_async, NotifySignal);

  quiddityManager = Switcher::make_switcher<SwitcherControllerLog>(name, this);

  quiddityManager->quids<MPtr(&Container::register_creation_cb)>(
      [this](qid_t id) {
        auto quid_name = quiddityManager->quids<MPtr(&Container::get_name)>(id);
        signal_cb(quid_name, "on-quiddity-created", std::vector<std::string>({quid_name}), this);
      });
  quiddityManager->quids<MPtr(&Container::register_removal_cb)>(
      [this](qid_t id) {
        auto quid_name = quiddityManager->quids<MPtr(&Container::get_name)>(id);
        signal_cb(quid_name, "on-quiddity-removed", std::vector<std::string>({quid_name}), this);
      });

  // do not play with previous config when saving
  quiddityManager->reset_state(false);
};

SwitcherController::~SwitcherController(){};

void SwitcherController::Init(Local<Object> exports) {
  Isolate *isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "Switcher"));

  // WARNING: Increment this Internal Field Count for each new method
  tpl->InstanceTemplate()->SetInternalFieldCount(35);

  // lifecycle - 1
  NODE_SET_PROTOTYPE_METHOD(tpl, "close", SwitcherClose);

  // nickname - 2
  NODE_SET_PROTOTYPE_METHOD(tpl, "set_nickname", SetNickname);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_nickname", GetNickname);

  // history - 5
  NODE_SET_PROTOTYPE_METHOD(tpl, "reset_command_history", ResetCommandHistory);
  NODE_SET_PROTOTYPE_METHOD(tpl, "save_history", SaveHistory);
  NODE_SET_PROTOTYPE_METHOD(tpl, "load_history_from_current_state", LoadHistoryFromCurrentState);
  NODE_SET_PROTOTYPE_METHOD(tpl, "load_history_from_scratch", LoadHistoryFromScratch);
  NODE_SET_PROTOTYPE_METHOD(tpl, "reset_history", ResetHistory);

  // life manager - 15
  NODE_SET_PROTOTYPE_METHOD(tpl, "load_defaults", LoadDefaults);
  NODE_SET_PROTOTYPE_METHOD(tpl, "load_bundles", LoadBundles);
  NODE_SET_PROTOTYPE_METHOD(tpl, "create", Create);
  NODE_SET_PROTOTYPE_METHOD(tpl, "remove", Remove);
  NODE_SET_PROTOTYPE_METHOD(tpl, "has_quiddity", HasQuiddity);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_classes_doc", GetClassesDoc);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_class_doc", GetClassDoc);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_quiddity_description", GetQuiddityDescription);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_quiddities_description", GetQuidditiesDescription);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_info", GetInfo);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_user_data", GetUserData);
  NODE_SET_PROTOTYPE_METHOD(tpl, "set_user_data", SetUserData);
  NODE_SET_PROTOTYPE_METHOD(tpl, "set_user_data_branch", SetUserDataBranch);
  NODE_SET_PROTOTYPE_METHOD(tpl, "remove_user_data", RemoveUserData);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_shmdata_path", GetShmdataPath);

  // properties - 2
  NODE_SET_PROTOTYPE_METHOD(tpl, "set_property_value", SetProperty);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_property_value", GetProperty);

  // methods - 5
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_methods_description", GetMethodsDescription);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_method_description", GetMethodDescription);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_methods_description_by_class", GetMethodsDescriptionByClass);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_method_description_by_class", GetMethodDescriptionByClass);
  NODE_SET_PROTOTYPE_METHOD(tpl, "invoke", Invoke);

  // property subscription - 3
  NODE_SET_PROTOTYPE_METHOD(tpl, "register_prop_callback", RegisterPropCallback);
  NODE_SET_PROTOTYPE_METHOD(tpl, "subscribe_to_property", SubscribeToProperty);
  NODE_SET_PROTOTYPE_METHOD(tpl, "unsubscribe_from_property", UnsubscribeFromProperty);

  // signal subscription - 2
  NODE_SET_PROTOTYPE_METHOD(tpl, "register_signal_callback", RegisterSignalCallback);
  NODE_SET_PROTOTYPE_METHOD(tpl, "subscribe_to_signal", SubscribeToSignal);

  // constructor
  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "Switcher"), tpl->GetFunction());
}

void SwitcherController::release() {
  is_closing_ = true;

  quiddityManager.reset();

  if (!uv_is_closing((uv_handle_t *)&switcher_log_async)) {
    uv_close((uv_handle_t *)&switcher_log_async, nullptr);
  }
  if (!uv_is_closing((uv_handle_t *)&switcher_prop_async)) {
    uv_close((uv_handle_t *)&switcher_prop_async, nullptr);
  }
  if (!uv_is_closing((uv_handle_t *)&switcher_sig_async)) {
    uv_close((uv_handle_t *)&switcher_sig_async, nullptr);
  }

  uv_mutex_destroy(&switcher_sig_mutex);
  uv_mutex_destroy(&switcher_prop_mutex);
  uv_mutex_destroy(&switcher_log_mutex);

  user_log_cb.Reset();
  user_prop_cb.Reset();
  user_signal_cb.Reset();
}

void SwitcherController::New(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);

  if (args.IsConstructCall()) {
    // Invoked as constructor: `new SwitcherController(...)`
    if (args.Length() < 2 || !args[0]->IsString() || !args[1]->IsFunction()) {
      isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
          isolate,
          "Wrong arguments. Switcher requires a name and logger callback.")));
      return;
    }

    // Construction
    Nan::Utf8String name((args.Length() >= 1 && !args[0]->IsString())
                               ? String::NewFromUtf8(isolate, "nodeserver")
                               : args[0].As<String>());
    SwitcherController *obj = new SwitcherController(
        std::string(*name), Local<Function>::Cast(args[1]));
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());

  } else {
    // Invoked as plain function `SwitcherController(...)`, turn into construct
    // call.
    const int argc = 1;
    Local<Value> argv[argc] = {args[0]};
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    args.GetReturnValue().Set(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
  }
}

Local<Value> SwitcherController::parseJson(Local<Value> jsonString) {
  Isolate *isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  Local<Object> global = isolate->GetCurrentContext()->Global();
  Local<Object> JSON =
      global->Get(String::NewFromUtf8(isolate, "JSON")).As<Object>();
  Local<Function> JSON_parse =
      Local<Function>::Cast(JSON->Get(String::NewFromUtf8(isolate, "parse")));
  return JSON_parse->Call(JSON, 1, &jsonString);
}

void SwitcherController::NotifyLog(uv_async_s *async) {
  Isolate *isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  SwitcherController *obj = static_cast<SwitcherController *>(async->data);
  bool cleanup = false;

  if (!obj->user_log_cb.IsEmpty()) {
    Local<Function> cb = Local<Function>::New(isolate, obj->user_log_cb);
    if (cb->IsCallable()) {
      TryCatch try_catch(isolate);
      uv_mutex_lock(&obj->switcher_log_mutex);
      // Performing a copy in order to avoid deadlock from log handlers
      // having to call the addon themselves
      auto log_list = obj->switcher_log_list;
      obj->switcher_log_list.clear();
      uv_mutex_unlock(&obj->switcher_log_mutex);

      for (auto &it : log_list) {
        Local<Value> argv[] = {String::NewFromUtf8(isolate, it.c_str())};
        cb->Call(isolate->GetCurrentContext()->Global(), 1, argv);
      }

      if (try_catch.HasCaught()) {
        FatalException(isolate, try_catch);
      }
    } else {
      cleanup = true;
    }
  } else {
    cleanup = true;
  }

  if (cleanup) {
    uv_mutex_lock(&obj->switcher_log_mutex);
    obj->switcher_log_list.clear();
    uv_mutex_unlock(&obj->switcher_log_mutex);
  }
}

void SwitcherController::NotifyProp(uv_async_s *async) {
  Isolate *isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  SwitcherController *obj = static_cast<SwitcherController *>(async->data);

  bool cleanup = false;

  if (!obj->user_prop_cb.IsEmpty()) {
    Local<Function> cb = Local<Function>::New(isolate, obj->user_prop_cb);
    if (cb->IsCallable()) {
      TryCatch try_catch(isolate);
      uv_mutex_lock(&obj->switcher_prop_mutex);
      // Performing a copy in order to avoid deadlock from prop handlers
      // having to call the addon themselves
      auto prop_list = obj->switcher_prop_list;
      obj->switcher_prop_list.clear();
      uv_mutex_unlock(&obj->switcher_prop_mutex);

      for (auto &it : prop_list) {
        Local<Value> argv[3];
        argv[0] = {String::NewFromUtf8(isolate, it.quid_.c_str())};
        argv[1] = {String::NewFromUtf8(isolate, it.prop_.c_str())};
        // argv[2] = {String::NewFromUtf8(isolate, it.val_.c_str())};

        // Try parsing as JSON, if it fails just return the string as-is
        Local<String> str_val = String::NewFromUtf8(isolate, it.val_.c_str());
        TryCatch val_try_catch(isolate);
        Local<Value> json = parseJson(str_val);
        if (!val_try_catch.HasCaught()) {
          argv[2] = {json};
        } else {
          argv[2] = {str_val};
        }

        cb->Call(isolate->GetCurrentContext()->Global(), 3, argv);
      }

      if (try_catch.HasCaught()) {
        FatalException(isolate, try_catch);
      }
    } else {
      cleanup = true;
    }
  } else {
    cleanup = true;
  }

  if (cleanup) {
    uv_mutex_lock(&obj->switcher_prop_mutex);
    obj->switcher_prop_list.clear();
    uv_mutex_unlock(&obj->switcher_prop_mutex);
  }
}

void SwitcherController::signal_cb(const std::string &quiddity_name,
                                   const std::string &signal_name,
                                   const std::vector<std::string> &params,
                                   void *user_data) {
  SwitcherController *obj = static_cast<SwitcherController *>(user_data);
  uv_mutex_lock(&obj->switcher_sig_mutex);
  obj->switcher_sig_list.push_back(
       SigUpdate(quiddity_name, signal_name, params));
  uv_mutex_unlock(&obj->switcher_sig_mutex);
  uv_async_send(&obj->switcher_sig_async);
}

void SwitcherController::NotifySignal(uv_async_s *async) {
  Isolate *isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  SwitcherController *obj = static_cast<SwitcherController *>(async->data);

  bool cleanup = false;

  if (!obj->user_signal_cb.IsEmpty()) {
    Local<Function> cb = Local<Function>::New(isolate, obj->user_signal_cb);

    if (!obj->user_signal_cb.IsEmpty() && cb->IsCallable()) {
      TryCatch try_catch(isolate);

      uv_mutex_lock(&obj->switcher_sig_mutex);
      // Performing a copy in order to avoid deadlock from signal handlers
      // having to call the addon themselves
      // For example, on-quiddity-removed has to also remove associated
      // quiddities
      auto sig_list = obj->switcher_sig_list;
      obj->switcher_sig_list.clear();
      uv_mutex_unlock(&obj->switcher_sig_mutex);

      for (auto &it : sig_list) {
        Local<Value> argv[3];
        Local<Array> array = Array::New(isolate, it.val_.size());
        for (auto &item : it.val_) {
          array->Set(0, String::NewFromUtf8(isolate, item.c_str()));
        }
        argv[0] = {String::NewFromUtf8(isolate, it.quid_.c_str())};
        argv[1] = {String::NewFromUtf8(isolate, it.sig_.c_str())};
        argv[2] = {array};

        cb->Call(isolate->GetCurrentContext()->Global(), 3, argv);
      }

      if (try_catch.HasCaught()) {
        FatalException(isolate, try_catch);
      }
    } else {
      cleanup = true;
    }
  } else {
    cleanup = true;
  }

  if (cleanup) {
    uv_mutex_lock(&obj->switcher_sig_mutex);
    obj->switcher_sig_list.clear();
    uv_mutex_unlock(&obj->switcher_sig_mutex);
  }
}

//  ██╗     ██╗███████╗███████╗ ██████╗██╗   ██╗ ██████╗██╗     ███████╗
//  ██║     ██║██╔════╝██╔════╝██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔════╝
//  ██║     ██║█████╗  █████╗  ██║      ╚████╔╝ ██║     ██║     █████╗
//  ██║     ██║██╔══╝  ██╔══╝  ██║       ╚██╔╝  ██║     ██║     ██╔══╝
//  ███████╗██║██║     ███████╗╚██████╗   ██║   ╚██████╗███████╗███████╗
//  ╚══════╝╚═╝╚═╝     ╚══════╝ ╚═════╝   ╚═╝    ╚═════╝╚══════╝╚══════╝

void SwitcherController::SwitcherClose(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());
  obj->release();
  return args.GetReturnValue().Set(Boolean::New(isolate, true));
}

//  ██╗  ██╗██╗███████╗████████╗ ██████╗ ██████╗ ██╗   ██╗
//  ██║  ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗╚██╗ ██╔╝
//  ███████║██║███████╗   ██║   ██║   ██║██████╔╝ ╚████╔╝
//  ██╔══██║██║╚════██║   ██║   ██║   ██║██╔══██╗  ╚██╔╝
//  ██║  ██║██║███████║   ██║   ╚██████╔╝██║  ██║   ██║
//  ╚═╝  ╚═╝╚═╝╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝   ╚═╝

void SwitcherController::ResetCommandHistory(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  obj->quiddityManager->reset_state(false);
}

void SwitcherController::SaveHistory(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String file_path(args[0].As<String>());
  if(fileutils::save(infotree::json::serialize(obj->quiddityManager->get_state().get()), std::string(*file_path))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::ResetHistory(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  obj->signal_muted_ = true;
  obj->quiddityManager->reset_state(true);
  obj->signal_muted_ = false;
}

void SwitcherController::LoadHistoryFromCurrentState(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String file_path(args[0].As<String>());
  obj->signal_muted_ = true;
  if (!obj->quiddityManager->load_state(
          infotree::json::deserialize(
              fileutils::get_content(std::string(*file_path))).get())) {
    obj->signal_muted_ = false;
    return args.GetReturnValue().Set(Boolean::New(isolate, false));
  }
  obj->signal_muted_ = false;

  return args.GetReturnValue().Set(Boolean::New(isolate, true));
}

void SwitcherController::LoadHistoryFromScratch(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String file_path(args[0].As<String>());

  obj->signal_muted_ = true;
  if (!obj->quiddityManager->load_state(
          infotree::json::deserialize(
              fileutils::get_content(std::string(*file_path))).get())) {
  obj->signal_muted_ = false;
    return args.GetReturnValue().Set(Boolean::New(isolate, false));
  }
  obj->signal_muted_ = false;

  obj->quiddityManager->reset_state(true);

  return args.GetReturnValue().Set(Boolean::New(isolate, true));
}

//   ██████╗ ██╗   ██╗██╗██████╗ ██████╗ ██╗████████╗██╗███████╗███████╗
//  ██╔═══██╗██║   ██║██║██╔══██╗██╔══██╗██║╚══██╔══╝██║██╔════╝██╔════╝
//  ██║   ██║██║   ██║██║██║  ██║██║  ██║██║   ██║   ██║█████╗  ███████╗
//  ██║▄▄ ██║██║   ██║██║██║  ██║██║  ██║██║   ██║   ██║██╔══╝  ╚════██║
//  ╚██████╔╝╚██████╔╝██║██████╔╝██████╔╝██║   ██║   ██║███████╗███████║
//   ╚══▀▀═╝  ╚═════╝ ╚═╝╚═════╝ ╚═════╝ ╚═╝   ╚═╝   ╚═╝╚══════╝╚══════╝

void SwitcherController::LoadDefaults(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher load_defaults: Wrong first arguments type")));
    return;
  }

  Nan::Utf8String file_path(args[0].As<String>());

  if (obj->quiddityManager->conf<MPtr(&Configuration::from_file)>(std::string(*file_path))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

/**
 * Load bundles from a JSON bundle description and make them available for creation
 * @method load_bundles
 * @param {string} description JSON description of the bundles to load
 * @return {bool} True if loading was successful, False if it wasn't
 * @throws {TypeError} Must be called with 1 string argument
 */
void SwitcherController::LoadBundles(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher load_bundles: Wrong first argument type")));
    return;
  }

  Nan::Utf8String bundles(args[0].As<String>());

  if (obj->quiddityManager->load_bundle_from_config(std::string(*bundles))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::Remove(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher remove: Wrong first arguments type")));
    return;
  }

  Nan::Utf8String first_arg(args[0].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*first_arg));
  if (qrox && obj->quiddityManager->quids<MPtr(&Container::remove)>(qrox.get_id())) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::HasQuiddity(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher has_quiddity: Wrong first arguments type")));
    return;
  }

  Nan::Utf8String quiddity_name(args[0].As<String>());

  if (0 != obj->quiddityManager->quids<MPtr(&Container::get_id)>(std::string(*quiddity_name))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::Create(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1 && args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "switcher create: Wrong first arg type")));
    return;
  }

  Nan::Utf8String first_arg(args[0].As<String>());

  if (args.Length() == 2) {
    if (!args[1]->IsString()) {
      isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
          isolate, "switcher create: Wrong second arg type")));
      return;
    }
  }

  Nan::Utf8String second_arg(args.Length() == 2 ? args[1].As<String>() : String::NewFromUtf8(isolate, ""));

  auto created = obj->quiddityManager->quids<MPtr(&Container::create)>(
    std::string(*first_arg), std::string(*second_arg), nullptr
  );

  return args.GetReturnValue().Set(String::NewFromUtf8(
    isolate, (created ? created.msg() : std::string()).c_str())
  );
}

void SwitcherController::GetInfo(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher get_info: Wrong first arg type")));
    return;
  }

  Nan::Utf8String first_arg(args[0].As<String>());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher get_info: Wrong second arg type")));
    return;
  }

  Nan::Utf8String second_arg(args[1].As<String>());
  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*first_arg));
  Local<String> res = String::NewFromUtf8(
      isolate, !qrox ? "":  qrox.get()->tree<MPtr(&InfoTree::serialize_json)>(std::string(*second_arg)).c_str());

  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetUserData(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher get_user_data: Wrong first arg type")));
    return;
  }

  Nan::Utf8String first_arg(args[0].As<String>());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher get_user_data: Wrong second arg type")));
    return;
  }

  Nan::Utf8String second_arg(args[1].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*first_arg));

  Local<String> res = String::NewFromUtf8(
      isolate, !qrox ? ""
                     : qrox.get()
                           ->user_data<MPtr(&InfoTree::serialize_json)>(
                               std::string(*second_arg))
                           .c_str());

  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::SetUserData(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 3) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }

  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: Wrong quiddity id arg type")));
    return;
  }

  Nan::Utf8String quiddity_name(args[0].As<String>());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: Wrong path arg type")));
    return;
  }

  auto qrox =
      obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(
          std::string(*quiddity_name));

  Nan::Utf8String path(args[1].As<String>());

  bool res = false;
  InfoTree::ptr tree;

  if (args[2]->IsString()) {
    tree = InfoTree::make(std::string(*Nan::Utf8String(args[2].As<String>())));
  } else if (args[2]->IsBoolean()) {
    tree = InfoTree::make(args[2].As<Boolean>()->Value());
  } else if (args[2]->IsInt32()) {
    tree = InfoTree::make(args[2].As<Int32>()->Value());
  } else if (args[2]->IsUint32()) {
    tree = InfoTree::make(args[2].As<Uint32>()->Value());
  } else if (args[2]->IsNumber()) {
    tree = InfoTree::make(args[2].As<Number>()->Value());
  }

  if (tree) {
    res = !qrox ? false
                : qrox.get()->user_data<MPtr(&InfoTree::graft)>(
                      std::string(*path), tree);
  }

  return args.GetReturnValue().Set(Boolean::New(isolate, res));
}

void SwitcherController::SetUserDataBranch(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 3) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }

  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: Wrong quiddity id arg type")));
    return;
  }

  Nan::Utf8String quiddity_name(args[0].As<String>());

  auto qrox =
      obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(
          std::string(*quiddity_name));

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: Wrong path arg type")));
    return;
  }

  Nan::Utf8String path(args[1].As<String>());

  bool res = false;
  InfoTree::ptr tree;

  if (args[2]->IsString()) {
    tree = infotree::json::deserialize(std::string(*Nan::Utf8String(args[2].As<String>())));
  }

  if (tree) {
    res = !qrox ? false
                : qrox.get()->user_data<MPtr(&InfoTree::graft)>(
                      std::string(*path), tree);
  } else {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: cannot deserialize json")));
    return;
  }

  return args.GetReturnValue().Set(Boolean::New(isolate, res));
}

void SwitcherController::RemoveUserData(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }

  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher remove_user_data: Wrong quiddity id arg type")));
    return;
  }

  Nan::Utf8String quiddity_name(args[0].As<String>());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher remove_user_data: Wrong path arg type")));
    return;
  }

  auto qrox =
      obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(
          std::string(*quiddity_name));

  Nan::Utf8String path(args[1].As<String>());

  bool res = false;
  InfoTree::ptr tree;

  res = !qrox ? false : static_cast<bool>(qrox.get()->user_data<MPtr(&InfoTree::prune)>(std::string(*path)));

  return args.GetReturnValue().Set(Boolean::New(isolate, res));
}

void SwitcherController::GetClassesDoc(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  Local<String> res = String::NewFromUtf8(
      isolate, infotree::json::serialize(obj->quiddityManager->factory<MPtr(&Factory::get_classes_doc)>().get()).c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetClassDoc(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String class_name(args[0].As<String>());

  Local<String> res = String::NewFromUtf8(
      isolate,
      infotree::json::serialize(
          obj->quiddityManager->factory<MPtr(&Factory::get_classes_doc)>()
              ->get_tree(std::string(".classes.") + std::string(*class_name))
              .get())
          .c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetQuiddityDescription(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String quiddity_name(args[0].As<String>());
  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*quiddity_name));

  Local<String> res = String::NewFromUtf8(
      isolate,
      !qrox ? ""
            : infotree::json::serialize(
                  obj->quiddityManager
                      ->quids<MPtr(&Container::get_quiddity_description)>(
                          qrox.get_id())
                      .get())
                  .c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetQuidditiesDescription(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());
  Local<String> res = String::NewFromUtf8(
      isolate,
      infotree::json::serialize(
          obj->quiddityManager->quids<MPtr(&Container::get_quiddities_description)>().get())
          .c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

//  ██████╗ ██████╗  ██████╗ ██████╗ ███████╗██████╗ ████████╗██╗███████╗███████╗
//  ██╔══██╗██╔══██╗██╔═══██╗██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██║██╔════╝██╔════╝
//  ██████╔╝██████╔╝██║   ██║██████╔╝█████╗  ██████╔╝   ██║   ██║█████╗  ███████╗
//  ██╔═══╝ ██╔══██╗██║   ██║██╔═══╝ ██╔══╝  ██╔══██╗   ██║   ██║██╔══╝  ╚════██║
//  ██║     ██║  ██║╚██████╔╝██║     ███████╗██║  ██║   ██║   ██║███████╗███████║
//  ╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝╚══════╝╚══════╝

void SwitcherController::SetProperty(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 3) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString() || !args[2]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String element_name(args[0].As<String>());
  Nan::Utf8String property_name(args[1].As<String>());
  Nan::Utf8String property_val(args[2].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*element_name));

  Local<Boolean> res = Boolean::New(
      isolate, !qrox ? false : qrox.get()->prop<MPtr(&property::PBag::set_str_str)>(
          std::string(*property_name),
          std::string(*property_val)));

  return args.GetReturnValue().Set(res);
}

void SwitcherController::GetProperty(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String element_name(args[0].As<String>());
  Nan::Utf8String property_name(args[1].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*element_name));

  Local<String> res = String::NewFromUtf8(
      isolate, !qrox ? "" : qrox.get()->prop<MPtr(&property::PBag::get_str_str)>(std::string(*property_name)).c_str());

  // Try parsing as JSON, if it fails just return the string as-is
  TryCatch try_catch(isolate);
  Local<Value> json = parseJson(res);
  if (!try_catch.HasCaught()) {
    return args.GetReturnValue().Set(json);
  } else {
    return args.GetReturnValue().Set(res);
  }
}

//  ███╗   ███╗███████╗████████╗██╗  ██╗ ██████╗ ██████╗ ███████╗
//  ████╗ ████║██╔════╝╚══██╔══╝██║  ██║██╔═══██╗██╔══██╗██╔════╝
//  ██╔████╔██║█████╗     ██║   ███████║██║   ██║██║  ██║███████╗
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║   ██║██║  ██║╚════██║
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║╚██████╔╝██████╔╝███████║
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝

void SwitcherController::Invoke(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() < 3) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString() || !args[2]->IsArray()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String element_name(args[0].As<String>());
  Nan::Utf8String method_name(args[1].As<String>());
  Local<Object> obj_arguments = args[2].As<Object>();
  Local<Array> arguments = obj_arguments->GetPropertyNames();

  auto tup_arg = std::string();
  for (unsigned int i = 0; i < arguments->Length(); i++) {
    Nan::Utf8String val(obj_arguments->Get(i).As<String>());
    if (tup_arg.empty()) tup_arg = serialize::esc_for_tuple(std::string(*val));
    else tup_arg = tup_arg + "," + serialize::esc_for_tuple(std::string(*val));
  }

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*element_name));
  if(!qrox) return;
  auto method_id = qrox.get()->meth<MPtr(&method::MBag::get_id)>(std::string(*method_name));
  if (0 == method_id) return;
  auto boollog = qrox.get()->meth<MPtr(&method::MBag::invoke_str)>(
      method_id,
      tup_arg);
  if (boollog) {
    Local<String> res = String::NewFromUtf8(isolate, boollog.msg().c_str());
    return args.GetReturnValue().Set(parseJson(res));
  }

  return;
}

void SwitcherController::GetMethodsDescription(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String element_name(args[0].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*element_name));

  Local<String> res = String::NewFromUtf8(
      isolate,
      !qrox ? "" : qrox.get()->tree<MPtr(&InfoTree::serialize_json)>(
          std::string(".method")).c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetMethodDescription(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String element_name(args[0].As<String>());
  Nan::Utf8String method_name(args[1].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*element_name));

  Local<String> res = String::NewFromUtf8(
      isolate,
      !qrox ? "" : qrox.get()->tree<MPtr(&InfoTree::serialize_json)>(
          std::string(".method.") + std::string(*method_name)).c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetMethodsDescriptionByClass(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String class_name(args[0].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::create)>(
      std::string(*class_name), std::string(), nullptr);
  if (!qrox) return args.GetReturnValue().Set(parseJson(String::NewFromUtf8(isolate,"")));

  Local<String> res =
      String::NewFromUtf8(isolate,
                          !qrox ? "" : qrox.get()->tree<MPtr(&InfoTree::serialize_json)>(
                              std::string(".method")).c_str());
  obj->quiddityManager->quids<MPtr(&Container::remove)>(qrox.get_id());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetMethodDescriptionByClass(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String class_name(args[0].As<String>());
  Nan::Utf8String method_name(args[1].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::create)>(
      std::string(*class_name), std::string(), nullptr);
  if (!qrox) return args.GetReturnValue().Set(parseJson(String::NewFromUtf8(isolate,"")));

  Local<String> res =
      String::NewFromUtf8(isolate,
                          !qrox ? "" : qrox.get()->tree<MPtr(&InfoTree::serialize_json)>(
                                  std::string(".method.") + std::string(*method_name))
                              .c_str());
  obj->quiddityManager->quids<MPtr(&Container::remove)>(qrox.get_id());
  return args.GetReturnValue().Set(parseJson(res));
}

/**
 * Get a shmdata path generated from a given quiddity and a suffix
 * @method get_shmdata_path
 * @param {string} quiddity_id The quiddity from which the shmdata has been generated
 * @param {string} shmdata_suffix The suffix of the generated shmdata
 * @return {string} The required shmdata path or an empty string if it doesn't exist
 * @throws {TypeError} It needs to be called with 2 string arguments
 */
void SwitcherController::GetShmdataPath(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *ctrl = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(
      Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")
      )
    );

    return;
  }

  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
      Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong arguments")
      )
    );

    return;
  }

  Nan::Utf8String quiddity_id(args[0].As<String>());
  Nan::Utf8String shmdata_suffix(args[1].As<String>());

  auto qrox = ctrl->quiddityManager->quids<MPtr(
    &Container::get_qrox_from_name)>(
      std::string(*quiddity_id)
    );

  if (!qrox) {
    return args.GetReturnValue().Set(
      String::NewFromUtf8(isolate, "")
    );
  }

  auto shmdata_path = qrox.get()->make_shmpath(std::string(*shmdata_suffix));
  Local<String> res = String::NewFromUtf8(isolate, shmdata_path.c_str());

  return args.GetReturnValue().Set(res);
}

//  ██████╗ ██████╗  ██████╗ ██████╗ ███████╗██████╗ ████████╗██╗   ██╗
//  ██╔══██╗██╔══██╗██╔═══██╗██╔══██╗██╔════╝██╔══██╗╚══██╔══╝╚██╗ ██╔╝
//  ██████╔╝██████╔╝██║   ██║██████╔╝█████╗  ██████╔╝   ██║    ╚████╔╝
//  ██╔═══╝ ██╔══██╗██║   ██║██╔═══╝ ██╔══╝  ██╔══██╗   ██║     ╚██╔╝
//  ██║     ██║  ██║╚██████╔╝██║     ███████╗██║  ██║   ██║      ██║
//  ╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝      ╚═╝

void SwitcherController::RegisterPropCallback(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  obj->user_prop_cb.Reset(isolate, Local<Function>::Cast(args[0]));

  return;
}

void SwitcherController::SubscribeToProperty(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String element_name(args[0].As<String>());
  Nan::Utf8String property_name(args[1].As<String>());


  auto qname = std::string(*element_name);
  auto pname = std::string(*property_name);
  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(qname);
  auto prop_id = !qrox ? 0 : qrox.get()->prop<MPtr(&property::PBag::get_id)>(pname);
  auto reg_id = !qrox ? 0 : qrox.get()->prop<MPtr(&property::PBag::subscribe)>(
      prop_id, [obj, qname, pname, prop_id, quid = qrox.get()]() {
        if (!obj->is_closing_) {
          uv_mutex_lock(&obj->switcher_prop_mutex);
          obj->switcher_prop_list.push_back(PropUpdate(
              qname, pname,
              quid->prop<MPtr(&property::PBag::get_str)>(prop_id)));
          uv_mutex_unlock(&obj->switcher_prop_mutex);
          uv_async_send(&obj->switcher_prop_async);
        }
      });
  if (0 == reg_id) {
    Local<Boolean> res = Boolean::New(isolate, false);
    return args.GetReturnValue().Set(res);
  }
  obj->prop_regs_[std::make_pair(qname, pname)] = reg_id;
  Local<Boolean> res = Boolean::New(isolate, true);
  return args.GetReturnValue().Set(res);
}

void SwitcherController::UnsubscribeFromProperty(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String element_name(args[0].As<String>());
  Nan::Utf8String property_name(args[1].As<String>());

  auto qname = std::string(*element_name);
  auto pname = std::string(*property_name);

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(qname);

  auto it = obj->prop_regs_.find(std::make_pair(qname, pname));
  if (obj->prop_regs_.end() == it) {
    Local<Boolean> res = Boolean::New(isolate, false);
    return args.GetReturnValue().Set(res);
  }
  Local<Boolean> res = Boolean::New(
      isolate,
      !qrox ? false : qrox.get()->prop<MPtr(&property::PBag::unsubscribe)>(
          qrox.get()->prop<MPtr(&property::PBag::get_id)>(pname),
          it->second));
  return args.GetReturnValue().Set(res);
}

//  ███████╗██╗ ██████╗ ███╗   ██╗ █████╗ ██╗
//  ██╔════╝██║██╔════╝ ████╗  ██║██╔══██╗██║
//  ███████╗██║██║  ███╗██╔██╗ ██║███████║██║
//  ╚════██║██║██║   ██║██║╚██╗██║██╔══██║██║
//  ███████║██║╚██████╔╝██║ ╚████║██║  ██║███████╗
//  ╚══════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝  ╚═╝╚══════╝

void SwitcherController::RegisterSignalCallback(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  obj->user_signal_cb.Reset(isolate, Local<Function>::Cast(args[0]));

  return;
}

void SwitcherController::SubscribeToSignal(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  Nan::Utf8String element_name(args[0].As<String>());
  Nan::Utf8String signal_name(args[1].As<String>());

  auto qrox = obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(std::string(*element_name));

  Local<Boolean> res = Boolean::New(
      isolate,
      0 !=
      !qrox ? 0 : qrox.get()->sig<MPtr(&signal::SBag::subscribe_by_name)>(
          std::string(*signal_name),
          [
              obj, quid = std::string(*element_name),
              sig = std::string(*signal_name)
           ](const InfoTree::ptr &tree) {
            if (!obj->signal_muted_)
              obj->signal_cb(quid, sig,
                             std::vector<std::string>(
                                 {tree->get_value().as<std::string>()}),
                             obj);
                  }));
  return args.GetReturnValue().Set(res);
}

void SwitcherController::SetNickname(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_nickname: Wrong first arg type")));
    return;
  }

  Nan::Utf8String first_arg(args[0].As<String>());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_nickname: Wrong second arg type")));
    return;
  }

  Nan::Utf8String second_arg(args[1].As<String>());

  auto qrox =
      obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(
          std::string(*first_arg));

  if (qrox.get()->set_nickname(std::string(*second_arg))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::GetNickname(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "switcher create: Wrong first arg type")));
    return;
  }

  Nan::Utf8String first_arg(args[0].As<String>());

  auto qrox =
      obj->quiddityManager->quids<MPtr(&Container::get_qrox_from_name)>(
          std::string(*first_arg));

  return args.GetReturnValue().Set(String::NewFromUtf8(
      isolate,
      !qrox ? "" : qrox.get()->get_nickname().c_str()));
}

SwitcherControllerLog::SwitcherControllerLog(SwitcherController *ctrl)
    : ctrl_(ctrl) {}

void SwitcherControllerLog::on_error(std::string &&str) {
  static std::string prefix = "switcher-error: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}

void SwitcherControllerLog::on_critical(std::string &&str) {
  static std::string prefix = "switcher-critical: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
void SwitcherControllerLog::on_warning(std::string &&str) {
  static std::string prefix = "switcher-warning: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
void SwitcherControllerLog::on_message(std::string &&str) {
  static std::string prefix = "switcher-message: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
void SwitcherControllerLog::on_info(std::string &&str) {
  static std::string prefix = "switcher-info: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
void SwitcherControllerLog::on_debug(std::string &&str) {
  static std::string prefix = "switcher-debug: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
