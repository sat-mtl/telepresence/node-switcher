#!/usr/bin/env node

const Switcher = require('node-switcher').Switcher

const sw = new Switcher('debug', console.log)

// DEBUG : trigger breakpoints from switcher-addons
sw.create('jacksrc')
