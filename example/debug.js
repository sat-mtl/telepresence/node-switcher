/**
 * Adapted from the default shell scripting for Switcher
 * @see {@link https://gitlab.com/sat-metalab/switcher/blob/master/doc/shell-scripting.md|shell scripting switcher and scenic}
 */

const Switcher = require('../build/Debug/switcher').Switcher
const sw = new Switcher('debug', console.log)

// create the quiddities
sw.create('videotestsrc', 'vid')
sw.create('glfwin', 'win')

// start the video with the "started" property set to true (will activate the video shmdata)
sw.set_property_value('vid', 'started', 'true')

// "connect" is a method, it needs to be invoked
sw.invoke('win', 'connect-quid', ['vid', 'video'])

// get shmdata path from quiddity vid
sw.get_shmdata_path('win', 'video')
