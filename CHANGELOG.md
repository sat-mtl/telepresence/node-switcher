Release notes
===================

Node-Switcher 3.2.0 (2020-11-13)
----------------------------------

* 🚑 Fix compilation on 18.04
* 🚑 Fix compilation with Switcher 2.1.24

Node-Switcher 3.1.2 (2020-10-30)
----------------------------------

* 🚑 Fix undeclared use of std::filesystem

Node-Switcher 3.1.1 (2020-03-30)
----------------------------------

* 🚑 Fix compilation on Switcher 2.1

Node-Switcher 3.1.0 (2020-02-24)
----------------------------------

* ✨ Add API call to load bundles from a JSON file

Node-Switcher 3.0.3 (2020-02-06)
----------------------------------

* ⬆️ Upgrade Switcher version

Node-Switcher 3.0.2 (2020-01-07)
-------------------------

* 🚑 Fix Switcher namespaces

Node-Switcher 3.0.1 (2020-01-06)
-------------------------

* 🚑 Fix broken Switcher includes

Node-Switcher 3.0.0 (2019-06-28)
-------------------------

Update V8 API for NodeJS 10 (LTS)
Add get_shmdata_path method into Switcher API

Node-Switcher 2.0.1 (2018-12-13)
-------------------------

Add CONTRIBUTING, CODE_OF_CONDUCT, AUTHORS and CODEOWNERS files
Refactor the README

Node-Switcher 2.0.0 (2018-11-28)
-------------------------

Node-Switcher separated from Scenic Core and put in its own repo
REPL interface
CI Pipeline and Docker image added
Fix NodeJS10 deprecation warnings and breaking changes
