ARG SWITCHER_IMAGE="registry.gitlab.com/sat-metalab/switcher"
ARG SWITCHER_TAG="master"

FROM ${SWITCHER_IMAGE}:${SWITCHER_TAG} AS dependencies
LABEL MAINTAINER="Scenic Dev <scenic-dev@sat.qc.ca>"

# Install common dependencies
RUN apt update -y \
    && DEBIAN_FRONTEND=noninteractive \
        apt install -y --no-install-recommends -qq \
            build-essential \
            pkg-config \
            curl \
            libglib2.0-dev \
            libjson-glib-dev \
            libgstreamer1.0-dev \
            git \
            libicu-dev \
            g++-8 \
            gcc-8

FROM dependencies AS nodejs
LABEL MAINTAINER="Scenic Dev <scenic-dev@sat.qc.ca>"

ARG NODEJS_VERSION="10"
ARG NODEJS_DOWNLOAD="https://deb.nodesource.com/setup_${NODEJS_VERSION}.x"

# Install NodeJS LTE
RUN curl -sL "${NODEJS_DOWNLOAD}" | bash - \
    && apt update -y \
    && DEBIAN_FRONTEND=noninteractive \
        apt install -y --no-install-recommends -qq \
            nodejs

FROM nodejs AS build
LABEL MAINTAINER="Scenic Dev <scenic-dev@sat.qc.ca>"

ARG BUILD_DIR="/opt/node-switcher"
ENV NODE_ENV="${NODE_ENV:-development}"

# Set scenic core paths
WORKDIR ${BUILD_DIR}
COPY . ${BUILD_DIR}

RUN npm install \
    && npm run build \
    # Install node-switcher REPL globally with unsafe permission
    && npm install -g --unsafe

FROM build AS clean
LABEL MAINTAINER="Scenic Dev <scenic-dev@sat.qc.ca>"

ARG BUILD_DIR="/opt/node-switcher"
ENV NODE_ENV="${NODE_ENV:-production}"

# Remove build folder and clean apt cache
RUN apt remove -y -qq \
        build-essential \
        pkg-config \
        curl \
        libglib2.0-dev \
        libjson-glib-dev \
        libgstreamer1.0-dev \
        git \
        libicu-dev \
        g++-8 \
        gcc-8 \
    # Clean apt cache
    && apt-get clean \
    && apt-get autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/